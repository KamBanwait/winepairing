// https://spoonacular.com/food-api/docs#Summarize-Recipe
let previousQuery = ''

// error handling for fetch
function handleErrors (response) {
  console.error(response)
}

// fetch using queryString passed in
async function getPairs (queryString) {
  const submitButton = document.getElementById('submitButton')
  const resultsContainer = document.getElementById('resultsContainer')

  await fetch(`../.netlify/functions/token-hider?queryString=${queryString}`)
    .then(res => {
      if (!res.ok) {
        return Promise.reject(res)
      }
      return res
    })
    .then(response => response.json())
    .then(data => {
      // if there's any paired wines returned
      if (data.pairedWines && data.pairedWines.length > 1) {
        submitButton.disabled = false
        submitButton.classList.remove('is-loading')
        displayResults(data, queryString)
      } else if (data.pairingText) {
        submitButton.disabled = false
        submitButton.classList.remove('is-loading')
        resultsContainer.innerHTML = `<p class="content">${data.pairingText}</p>`
      } else {
        resultsContainer.innerHTML = data.message ? data.message : '<p class="content">We didn\'t find any results, you may have spelt something wrong or you need to break down the food in to something more specific</p>'
        submitButton.disabled = false
        submitButton.classList.remove('is-loading')
      }
    })
    .catch(err => {
      handleErrors(err)
    })
}

// display results
function displayResults (resultsJSON, queryString) {
  const resultsContainer = document.getElementById('resultsContainer')

  // reset results html
  resultsContainer.innerHTML = ''

  const pairedWinesArray = resultsJSON.pairedWines
  let pairedWinesList = ''

  const productRecommendation = `
    <div class="content column is-5 is-offset-1">
      <h4 class="title is-4">I recommend a bottle of: <em>${resultsJSON.productMatches[0].title}</em></h4>
      <div class="content">
        <p>${resultsJSON.productMatches[0].description && resultsJSON.productMatches[0].description && resultsJSON.productMatches[0].description && resultsJSON.productMatches[0].description}</p>

        <p>Price: <strong>${resultsJSON.productMatches[0].price}</strong></p>

        <p>Avg rating: ${resultsJSON.productMatches[0].averageRating && Math.round(resultsJSON.productMatches[0].averageRating)}</p>

        <p>Rating Count: ${resultsJSON.productMatches[0].ratingCount && resultsJSON.productMatches[0].ratingCount}</p>

        <p>Score: ${Math.round(resultsJSON.productMatches[0].score && resultsJSON.productMatches[0].score)}</p>
      </div>
      <div class="content">
        <img src="${resultsJSON.productMatches[0].imageUrl}" alt="Placeholder image" class="recommendation" width="200">
      </div>
      <div class="content">
        <p><a href="${resultsJSON.productMatches[0].link}" target="_blank">...Read more...</a></p>
      </div>
    </div>
  `

  pairedWinesArray.forEach(wine => {
    pairedWinesList += `<li>${wine}</li>`
  })

  resultsContainer.innerHTML += `
    <h3 class="column is-12 title is-3">My recommendations for '<em>${queryString}</em>':</h3>
    <div class="column is-6">
      <div class="content">
        <h4 class='title is-4'>Paired wines:</h4>
        <ol>${pairedWinesList}</ol>
      </div>
      <div class="content">
        <h4 class="title is-4">Why these?</h4>
        <p class="content">${resultsJSON.pairingText}</p>
      </div>
    </div>
  `

  resultsContainer.innerHTML += productRecommendation
}

document.getElementById('findWines') && document.getElementById('findWines').addEventListener('submit', event => {
  event.preventDefault()

  // set button to disabled state
  const submitButton = document.getElementById('submitButton')
  const newQuery = event.target.querySelector('input').value

  if (newQuery !== '' || newQuery !== previousQuery) {
    submitButton.classList.add('is-loading')
    submitButton.disabled = true
    previousQuery = newQuery
    getPairs(newQuery)
  } else {
    console.info(`${newQuery} is the same as the previous Query; ${previousQuery}`)
  }
})
