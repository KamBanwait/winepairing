const axios = require('axios')

exports.handler = async function(event, context) {
  const { API_TOKEN, API_URL } = process.env
  let queryString = event.queryStringParameters.queryString
  const URL = `${API_URL}${queryString}&apiKey=${API_TOKEN}`

  try {
    const { data } = await axios.get(URL)
    return {
      statusCode: 200,
      body: JSON.stringify(data)
    }
  } catch (error) {
    const { status, statusText, headers, data } = error.response
    return {
      statusCode: error.response.status,
      body: JSON.stringify({ status, statusText, headers, data })
    }
  }
}
